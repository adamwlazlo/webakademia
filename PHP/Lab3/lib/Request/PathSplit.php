<?php

class PathSplit
{
    //public static function patch(int $id): string - PHP7
    public static function patch($id)
    {
        $id = intval($id);
        if($id > 999999999)
        {
            //TODO: throw new exeption

        }
        $result = strval($id);
        $result = str_pad($result,9,'0',STR_PAD_LEFT);
        $result = str_split($result,3);
        $result = implode('/', $result);

        return $result;
    }
}

var_dump(PathSplit::patch(1213));