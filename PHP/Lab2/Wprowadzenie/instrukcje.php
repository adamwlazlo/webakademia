<?php
/*
if(1) {
    echo ''; 
}
 if(1) echo ''; // błędny zapis, zawsze musza by klamry
 
 switch (@zmienna) {  //switch umożliwa wykonanie większej niż jedną instrukcję
     case 1:
     case 2:
     case 3:
         //instrukcja
         break;
     default:
         break;
 }
  */
 $array = [1, 2, 3, 4, 5, 6, 7, 8];
 //$array = array();

 foreach($array as $val)
 {
     echo '<br>';
     echo $val;
     echo '<br>';
 } 
 
 foreach($array as $key => $val)
 {
     //echo "$key => $val <br><br>";
     
     echo $key .' => '. $val .'<br>';
 }
 
