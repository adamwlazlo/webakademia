<?php 

namespace Database;

class Connection
{
    protected $host;
    protected $port;
    protected $login;
    protected $password;
    protected $databaseName;
   
    /**
     *
     * @var \PDO
     */
   
   
    protected $connection;
   
    /**Creates PDO connection object
     * *@param string $host
     *
     *
     * @param string $dbname
     */
   
   
   
    public function __construct( $host, $port, $login, $password, $dbname)
    {
        $this->host = $host;
        $this->port = $port;
        $this->login = $login;
        $this->password = $password;
        $this->databaseName = $dbname;
        //$cd = 'mysql:dbname=testdb;host=127.0.0.1';
       
        $this->connect();
    }
   
    public function connect()
    {
        if(!($this->connection instanceof \PDO))
        {
        $cd = 'mysql:dbname=' . $this->databaseName . ';host=' . $this->host . ':' . $this->port;
       
        try
        {
           $this->connection = new \PDO($cd, $this->login, $this->password);
        }
        catch (\PDOException $pdoexp)
        {
            dump($pdoexp->getMessage());
            //TODO: obsługa wyjątku
            //zalogowac
        }
    }
    }
   
        public function reconnect()
    {
        //wznowienie polączenia do bazy danych
            $this->disconnect();
            $this->connect();
    }
   
         public function disconnect()
    {
        $this->connection = null;
    }
         
    public function querry($sql, $params = [])
    {
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        return $stmt;
        
    }
    
    
    
}