<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Database;

/**
 * Description of Multiconnector
 *
 * @author WSIiZ
 */
class Multiconnector extends Connection
{

     protected static $singletone = null;
    protected $connection;


    public $pole;
    
    private function __construct()
    {
        $this->connection = Connection(\Config::DB_HOST,
                \Config::DB_PORT,
                \Config::DB_NAME,
                \Config::DB_LOHIN,
                \Config::DB_PASSWORD);
    }
    
    public function get($connectionName)
    {
        
        if(!(static::$singleton instanceof self))
        {
            static::$singleton = new self();
        }
        
        return $this->connection;
    }
    
}
