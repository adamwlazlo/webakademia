<?php

function dump($var)
{
    $var = print_r($var, true);
    echo '<pre>' . $var . '</pre>';
}

function autoload($class)
{
    $path = explode('\\', $class);
    $path = 'lib/' . implode('/', $path) . '.php';
    require_once $path;
}

spl_autoload_register('autoload');

function db()
{
    global $db;
    if (!($db instanceof \Database\Connection))
    {
        $db = new \Database\Connection('localhost', '3306', 'root', '', 'employees');
    }
    return $db;
}