﻿// załadowanie modułu Gulp
var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var combineMq = require('gulp-combine-mq');
var imagemin = require('gulp-imagemin');



//Domyślne zadanie
gulp.task('default', function() {
		return gulp.src([
		'node_modules/jquery/dist/jquery.js',
		'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
		'assets/app.js'
	]).pipe(concat('app.js')).pipe(gulp.dest('public'));
});

gulp.task('sass', function() {
		return gulp.src('assets/app.scss').pipe(sass().on('error', sass.logError)).pipe(gulp.dest('public'));
});

gulp.task('watch', function() {
		gulp.watch(['assets/*.scss', 'assets/*/*.scss'], ['sass']);
});

gulp.task('sass', function() {
  return gulp.src('assets/app.scss')
  .pipe(sass().on('error', sass.logError))
  //.pipe(cssmin()) //minimalizuje css, zakomentowane cofa zmiany
  .pipe(gulp.dest('public'));
});

gulp.task('default', function() {
  return gulp.src([
      'node_modules/jquery/dist/jquery.js',
'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
      'assets/app.js'
  ])
  .pipe(concat('app.js'))
  //.pipe(uglify()) //minimalizuje js, zakomentowane cofa zmiany
  .pipe(gulp.dest('public'));
});

gulp.task('sass', function() {
  return gulp.src('assets/app.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(combineMq({
      beautify: true
  }))
  .pipe(gulp.dest('public'));
});

gulp.task('images', function(){
  gulp.src('assets/images/*')
      .pipe(imagemin())
      .pipe(gulp.dest('public/images'))
});