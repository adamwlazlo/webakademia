function isEmpty(str)
{
	if (str.length == 0)
		return true
	else
		return false
}

function isWhiteCharacter(str)
{
	var ws = "\t\n\r "
	for (i = 0; i < str.length; i++)
		{
			var c = str.charAt(i)
			if ( ws.indexOf(c) == -1)
			return false
		}
	return true
}

function checkField(str, msg)
{
	if (isWhiteCharacter(str) || isEmpty(str))
	{
		alert(msg);
		return false;
	}
	return true;
}
 
function checking(form)
{
return 	checkStringElem(form.elements["f_imie"], ' Błędne imię') && 
		checkStringElem(form.elements["f_nazwisko"], ' Błędne nazwisko') && 
		checkStringElem(form.elements["f_ulica"], ' Błędna ulica/osiedle') && 
		checkStringElem(form.elements["f_miasto"], ' Błędne miasto') && 
		emailRegEx(form.elements["f_email"].value);
}
 
function emailRegEx(str)
{
	var mail = /[a-zA-Z_0-9\.]+@[a-zA-Z_0-9\.]+\.[a-zA-Z][a-zA-Z]+/
	if(mail.test(str))
	{
		return true;
	}
	
	alert("Podaj poprawny mail");
	return false;
}
 
function errorClear(objName)
{
	document.getElementById(objName).innerHTML = "";
}

function checkStringElem(obj, msg)
{
	var str = obj.value;
	var errorFieldName = "e_" + obj.name.substr(2,obj.name.length);

	if(isWhiteCharacter(str) || isEmpty(str))
	{
		document.getElementById(errorFieldName).innerHTML = msg;
		obj.focus();
		return false;
	}
	errorClear(errorFieldName);
	return true;
}


function showElem(e)
{
	document.getElementById(e).style.visibility = 'visible';
}
	
function hideElem(e)
{
	document.getElementById(e).style.visibility = 'hidden';
}