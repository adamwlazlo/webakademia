import { Component, OnInit } from '@angular/core';
import {DatabaseService} from '../database.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartProduct;
  constructor(private _data : DatabaseService) { }

  ngOnInit() {
    this.cartProduct = this._data.cartList;
  }
  removeFromCart(i){
    this._data.removeFromCart(i);
  }

}
