import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products;
  constructor(private _data: DatabaseService) { }

  ngOnInit() {
    this._data.newData.subscribe(res=>{this.products = res;})
  }
  addToCart(id){
    this._data.addToCart(id);
  }

}
