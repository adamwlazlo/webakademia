import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Route } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {

  product;
  constructor(private _data: DatabaseService, private route: ActivatedRoute) {
    this.route.params.subscribe(res=>{
      this.product = this._data.findProduct(res.id);
    })
   }

  ngOnInit() {
    console.log(this.product);
  }
  addToCart(id){
    this._data.addToCart(id);
  }

}
