import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
 
@Injectable()
export class DatabaseService {
  url = {
    database:{
            'post':'http://api.webakademia.it/post.php',
            'get':'http://api.webakademia.it/get.php',
            'delete':'http://api.webakademia.it/delete.php?id=',
            'user':'http://api.webakademia.it/user.php'
        }
  }
  private dataSheet = new BehaviorSubject<any>([]);
  newData = this.dataSheet.asObservable();
  cartList = [];

  constructor(private http : HttpClient) {
      this.http.post(this.url.database.get,{user_id: 1}).subscribe(res=>{
      this.dataSheet.next(res);
    }) }

  addToCart(id){
    this.cartList.push(this.findProduct(id));
  }
  
  findProduct(id_product){
    let data;
    this.newData.subscribe(res=>{
     data =  res.find(element=>{
        return element.id == id_product;
      });
    });
    return data;
  }
  removeFromCart(i){
    this.cartList.splice(i, 1);
  }

}
