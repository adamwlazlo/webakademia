app.service('Database', function ($http, URL, USER) {
    this.checkUser = function(data){
        data = Object.assign({}, data);
        return $http({
            method: "POST",
            url: URL.database.user,
            data:data
            });
       };
    this.add = function (data) {
        data['user_id'] = USER.id;
        data = Object.assign({}, data);
        return $http({
            method: "POST",
            url: URL.database.post,
            data:data
            });
       };
    this.get = function(id_product){
        id_product = typeof id_product != 'undefined' ? id_product: 0;
        var data = [];
        console.log(USER.id);
        data['user_id'] = USER.id;

//przekazanie do API
        if(id_product)
          data['id'] = id_product;

        data = Object.assign({}, data);
        return $http({
            method: "POST",
            url: URL.database.get,
            data:data
        })
    };
});
