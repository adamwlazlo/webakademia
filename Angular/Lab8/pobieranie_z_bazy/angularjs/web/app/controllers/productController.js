app.controller('productController', function($scope, Cart, $routeParams, Database){

  Database.get($routeParams.id_product).then(function(response){
    console.log(response.data[0]);
    $scope.product = response.data[0];
  })

  $scope.addProduct = function(product)
  {
      Cart.addToCart(product);
  }
});
