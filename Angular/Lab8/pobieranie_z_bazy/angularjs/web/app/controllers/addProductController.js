app.controller('addProductController', function($scope, Database, $timeout){
  $scope.submited = 0;
  $scope.formData = [];
  $scope.saveProduct = function(valid)
  {
      $scope.submited = 1;
      if(valid)
      {
          Database.add($scope.formData)
          .then(function(response){
              $scope.showMessage = JSON.parse(JSON.parse(response.data));
              if($scope.showMessage)
              {
                  $scope.formData = [];
                  $scope.submited = 0;
              }
              $timeout(function(){
                  $scope.showMessage = null;
              }, 5000);
          });
      }
  }
});
