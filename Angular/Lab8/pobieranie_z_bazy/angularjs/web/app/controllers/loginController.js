app.controller('loginController', function($scope,Database, USER, $location, $cookies){
 $scope.loginFormData = [];
 $scope.submited = 0;
 $location.url('/');
 var dataCookie = $cookies.get('webakademia_id');
 if(!dataCookie)
 {
     jQuery('.loginModal').modal('show');
 }
 else
 {
     dataCookie = JSON.parse(dataCookie);
     USER.id = dataCookie.id;
     USER.name = dataCookie.name;
     USER.surname = dataCookie.surname;
     $location.url('logged');
 }

 $scope.login = function(valid)
 {
     $scope.submited = 1;
     if(valid)
     {
         Database.checkUser($scope.loginFormData)
         .then(function(response){
            USER.id = response.data.id;
            USER.surname = response.data.surname;
            USER.name = response.data.name;
             jQuery('.loginModal').modal('hide');
             $cookies.put('webakademia_id', JSON.stringify(USER));
             $location.url('logged');
         });
     }
 }
});
