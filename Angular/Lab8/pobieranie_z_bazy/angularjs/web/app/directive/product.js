app.directive('myProduct', function(URL){
    return {
        restrict: 'E',
        templateUrl: URL.partial+'product.html',
        scope: {
            product: '='
        },
        replace: true,
        controller: 'listProduct'
    }
});
