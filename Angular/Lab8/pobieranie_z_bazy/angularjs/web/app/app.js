'use strict';


var app = angular.module('myApp', ["ngRoute", "ngAnimate", "ngCookies"])
.constant('URL',
    {
        'partial':'view/partials/',
        'site':'view/sites/',
        'database':{
            'post':'http://api.webakademia.it/post.php',
            'get':'http://api.webakademia.it/get.php',
            'delete':'http://api.webakademia.it/delete.php?id=',
            'user':'http://api.webakademia.it/user.php'
        }
    })
.constant('USER', {'id':0, 'name':'', 'surname':''})
.config(function ($routeProvider, URL, $httpProvider) {
    $routeProvider.when("/product/:id_product", {
        templateUrl: URL.site+"product.html",
        controller: 'productController'
    });
    $routeProvider.when("/addproduct/", {
        templateUrl: URL.site+"addproduct.html",
        controller: 'addProductController'
    });
    $routeProvider.otherwise({
        templateUrl: URL.site+'list.html',
        controller: 'listProduct'
    });
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
})
