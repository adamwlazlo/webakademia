app.controller('kontroler', function($scope){
  $scope.welcome = "Witaj AngularJS";
  $scope.welcome2 = "To jest mój projekt";
  $scope.wiadomosc = 'ng-click';
  $scope.copied = '';
  $scope.copied2 = 'Pokaż wiadomość';
  $scope.pokazAlert = function()
  {
    $scope.welcome2 = "To jest zmieniony tekst po kliknięciu przcisku";
  }
  $scope.mojNgChange = function()
  {
    $scope.wiadomosc = 'ng-change';
    alert();
  }
  $scope.kopiowanie = function()
  {
    $scope.copied = 'Właśnie skopiowałeś tekst :)';
    $scope.copied2 = 'Właśnie skopiowałeś tekst :)';
  }
  
  $scope.dblClick = function()
  {
   if (($scope.copied2 == 'Pokaż wiadomość') || ($scope.copied2 == 'Właśnie skopiowałeś tekst :)')) 
   {
      $scope.copied2 = 'Po co tyle klikasz?';
   }
   else
   {
      $scope.copied2 = 'Pokaż wiadomość';
   }
  }
});
