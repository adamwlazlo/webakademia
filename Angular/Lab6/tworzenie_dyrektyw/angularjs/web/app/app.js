'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [])
.constant('URL', {'partial':'view/partials/'});
