'use strict';


var app = angular.module('myApp', ["ngRoute"])
.constant('URL', {'partial':'view/partials/', 'site':'view/sites/'})
.config(function ($routeProvider, URL) {
    $routeProvider.when("/product/:id_product", {
        templateUrl: URL.site+"product.html",
        controller: 'productController'
    });
    $routeProvider.when("/products", {
        templateUrl: URL.site+"products.html",
        controller: 'productsController'
    });
    $routeProvider.when("/summary", {
        templateUrl: URL.site+"summary.html",
        controller: 'summaryController'
    });
    $routeProvider.otherwise({
        templateUrl: URL.site+'list.html',
        controller: 'listProduct'
    });
})
