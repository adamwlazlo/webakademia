app.service('Cart', function () {
    var listProduct = [];


    this.addToCart = function(item)
    {
        if (listProduct.indexOf(item) < 0 )
        {
          item.licznik=1;
          listProduct.push(item);
        }
        else
        {
          item.licznik++;
        }
    }

    this.getListProduct = function()
    {
        return listProduct;
    }
});
